{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p style=\"text-align:right;\">Daniel Böckenhoff, Max-Planck-Institute for Plasmaphysics</p>\n",
    "\n",
    "## Neural Networks to recognise numbers with the MNIST database\n",
    "The [MNIST](http://yann.lecun.com/exdb/mnist/) database (Modified National Institute of Standards and Technology database) is a large database of handwritten digits that is commonly used for training various image processing systems. The database is also widely used for training and testing in the field of machine learning.\n",
    "In this notebook we will perform image classification with shallow neural networks (NNs) on the basis of this data set.\n",
    "\n",
    "**What you will learn:**\n",
    " - How to build data sets for [tensorflow](https://www.tensorflow.org/) (google machine learning api)\n",
    " - How to build neural networks with [keras](https://www.tensorflow.org/api_docs/python/tf/keras)\n",
    " - Monitoring training progress (callbacks)\n",
    " - Saving and reading a model to and from a file\n",
    " - Classification error analysis\n",
    " - Network architecture optimization\n",
    " \n",
    "**Task:**\n",
    "Carefully read the cells and execute them (Shift enter or menue panel). Complete the exercises marked as such."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pathlib\n",
    "import tensorflow as tf\n",
    "import numpy as np\n",
    "import datetime\n",
    "import os\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Image Data Preparation\n",
    "We will load the mnist data set with the *keras.datasets* backend."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the path to the directory where we have already downloaded the data\n",
    "data_path = (pathlib.Path(\".\") / \"..\").resolve() / 'data' / 'mnist.npz'  # if no path were given, keras would download the dataset\n",
    "\n",
    "# Load the data\n",
    "data = np.load(data_path)\n",
    "x_train, y_train, x_test, y_test = data['x_train'], data['y_train'], data['x_test'], data['y_test']\n",
    "\n",
    "class_names = list(set(y_train))\n",
    "num_classes = len(class_names)  # 10\n",
    "num_imgs = len(y_train) + len(y_test)\n",
    "\n",
    "print(f\"Number of samples in the whole data set: {num_imgs}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot a few sample images for each class\n",
    "num_examples_per_class = 4\n",
    "_, ax = plt.subplots(nrows = num_classes, ncols = num_examples_per_class,\n",
    "                     figsize = (3 * num_examples_per_class, 3 * num_classes)) # adjust size here\n",
    "    \n",
    "for i in range(num_classes): # loop over classes = rows\n",
    "    indices = np.where(y_train == i)\n",
    "    for j, img in enumerate(x_train[indices][:num_examples_per_class]):\n",
    "        ax[i, j].imshow(img, cmap='Greys')\n",
    "        ax[i, j].set_title(\"class \" + str(i), fontsize=15)  \n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Make TensorFlow Datasets\n",
    "\n",
    "\n",
    "**Note:** Training, Validation and Test Split\n",
    "A rule of thum is to split the data set in taining, validation and test set by the ratio 80:10:10.\n",
    "A specialty in the mnist dataset is the predefined test set which serves the purpose of exact comparability between different research groups.\n",
    "\n",
    "NNs cannot work with letter sequences directly so we have to think about how to encode the sequences (and our labels) into numerical form.\n",
    "### One hot encoding\n",
    "A popular method for encdoding categorical features that avoids an implicit order is \"one hot encoding\" (implemented with keras.utils.to_categorical)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "assert x_train.shape[1] == x_train.shape[2], \"Expected square image\"\n",
    "img_size = x_train.shape[1]  # width and height of all images\n",
    "\n",
    "# Convert target classes to categorical ones\n",
    "y_train_labels = y_train\n",
    "y_test_labels = y_test\n",
    "y_train = tf.keras.utils.to_categorical(y_train_labels, num_classes)\n",
    "y_test = tf.keras.utils.to_categorical(y_test, num_classes)\n",
    "\n",
    "# Let us inspect what to_categorical did:\n",
    "for i in range(10):\n",
    "    print(\"Target label: {}, One hot encoded Target Vector: {}\"\n",
    "          .format(y_train_labels[i], y_train[i]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Performance benchmarks\n",
    "**Exercise:**\n",
    "1. Before we build a machine learning model to solve the task properly, lets first determine the performance of two naive benchmarks. What $$\\text{Accuracy} = \\frac{\\text{#correctly classified training data}}{\\text{#evaluated training data}}$$ would you expect ...\n",
    "    1. ... for a random output generator (assigning images to random classes)?\n",
    "    2. ... for a naive algorithm which works without seeing any picture but knows about the number of samples per class. What would this \"algorithm\" predict?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "task"
    ]
   },
   "outputs": [],
   "source": [
    "# Your code here for Exercise 1.B\n",
    "# ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "fcnn"
    ]
   },
   "source": [
    "## Define and Train a Fully Connected Neural Network\n",
    "\n",
    "**Exercise:**\n",
    "\n",
    " 2. ***Build a [sequential model](https://www.tensorflow.org/api_docs/python/tf/keras/Sequential) of the following neural network architecture with the tf.keras library:***\n",
    "\n",
    "    * An [Input Layer](https://www.tensorflow.org/api_docs/python/tf/keras/Input) for an image input\n",
    "    * We are completely neglecting the relations between pixels & color channels, so we [flatten](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Flatten) the input\n",
    "    * One [dense](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Dense) hidden layer with 50 neuron units (use linear activation first)\n",
    "    * A dense output layer with 10 units. Make sure to use the proper activation function for a classification  output layer.\n",
    "    * [Compile](https://www.tensorflow.org/api_docs/python/tf/keras/Sequential#compile) the model and specify:\n",
    "        * loss: Use the categorical cross entropy loss function $$\\mathcal{L} = -\\sum_{i=1}^{\\text{\\# outputs}}t_i \\cdot \\log{y_i}$$, where $y_i$ is the $i$-th scalar value in the model output and $t_i$ is the corresponding target value. This loss is a very good measure of how distinguishable two discrete probability distributions are from each other. In this context, $y_i$ is the probability that event $i$ occurs, i.e. it is normalized such that $\\sum_{i=1}^{\\text{\\# outputs}}y_i = 1$. The minus sign ensures that the loss gets smaller when the distributions get closer to each other.\n",
    "        * optimizer: [Adam algorithm](https://arxiv.org/pdf/1412.6980.pdf)\n",
    "        * metrics: 'accuracy' (loss is part of the metrics by default)\n",
    "\n",
    "**Tip:**\n",
    "* Use the \"add()\" method of the sequential model\n",
    "* change the model_name for every model you compile (and train) and choose names that you can remember properly. Possibly make lab notes to remember what you changed from one model to the other.\n",
    "* Use model.summary() for a pretty printed summary of the layer types and dimensions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "task"
    ]
   },
   "outputs": [],
   "source": [
    "# sequential stack of network layers\n",
    "# Note: The name variable controls the saving and loading, so change to a meaningful name\n",
    "#     for every experiment you do in model training.\n",
    "model = tf.keras.models.Sequential(name=\"mnist_dense_student\")\n",
    "\n",
    "# Your code here for Exercise 2\n",
    "# ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise:**\n",
    "\n",
    " 3. Let us get a feeling for the metrics we are going to use to evaluate the performance of our classification.\n",
    "     1. What accuracy do you expect at the beginning of the training?\n",
    "     2. What loss do you expect at the beginning of the training?\n",
    "\n",
    "For both metrics: Argue theoretically and check your expectation empirically in the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "task"
    ]
   },
   "outputs": [],
   "source": [
    "# Let us check the precision of the initial model (with random weights)\n",
    "def reinitialize_random_weights(model):\n",
    "    \"\"\"\n",
    "    This function can be used to set new random weights to all layers of the model.\n",
    "    Use this to evaluate multiple freshly initialized models for statistics.\n",
    "    \"\"\"\n",
    "    for ix, layer in enumerate(model.layers):\n",
    "        if hasattr(model.layers[ix], 'kernel_initializer') and \\\n",
    "                hasattr(model.layers[ix], 'bias_initializer'):\n",
    "            weight_initializer = model.layers[ix].kernel_initializer\n",
    "            bias_initializer = model.layers[ix].bias_initializer\n",
    "\n",
    "            old_weights, old_biases = model.layers[ix].get_weights()\n",
    "\n",
    "            model.layers[ix].set_weights([\n",
    "                weight_initializer(shape=old_weights.shape),\n",
    "                bias_initializer(shape=len(old_biases))])\n",
    "\n",
    "# Your code here\n",
    "# Hint: Use the function call below to retrieve performance metrics of the model.\n",
    "#       However, you want some statistics with multiple initializations of the same model.\n",
    "# Note: For now, just accept that we normalize x_test to 0 mean and variance ~= 1\n",
    "test_loss, test_acc = model.evaluate((x_test - np.mean(x_test)) / np.var(x_test - np.mean(x_test)),\n",
    "                                     y_test,\n",
    "                                     verbose=0)\n",
    "print(test_loss, test_acc)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Since we want to experiment with multiple networks, we abstract the training process such that you can\n",
    "# easily run training as a function.\n",
    "log_basedir = os.path.join(\"logs\", \"mnist_dense/\")\n",
    "\n",
    "def train(model, x_train, y_train, epochs=20, batch_size=250, log_basedir=log_basedir):\n",
    "    # Callbacks: What should be done during (long) training?\n",
    "\n",
    "    # Function to store model to file, if validation loss has a new record\n",
    "    # Check always after having seen at least another save_freq examples.\n",
    "    model_file_name = model.name + '.keras'\n",
    "    checkpoint = tf.keras.callbacks.ModelCheckpoint(\n",
    "        model_file_name,\n",
    "        save_weights_only=False,  # save the entire model, not only the weights. This allows re-building the whole model\n",
    "        monitor='val_loss',\n",
    "        mode='min',\n",
    "        save_best_only=True,\n",
    "        verbose=1)\n",
    "\n",
    "    # Function to decrease learning rate by 'factor'\n",
    "    # when there has been no significant improvement in the last 'patience' epochs.\n",
    "    reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(\n",
    "        monitor='val_loss', mode='min', factor=0.75, patience=3, verbose=1)\n",
    "\n",
    "    # Use tensorboard to show the progress. See cell below for instructions on how to use tensorboard\n",
    "    log_dir = log_basedir + datetime.datetime.now().strftime(\"%Y%m%d-%H%M%S\") + \"-\" + model.name\n",
    "\n",
    "    tensorboard_callback = tf.keras.callbacks.TensorBoard(\n",
    "        log_dir,\n",
    "        histogram_freq=1,  # frequency (in epochs) at which to compute activation and weight histograms for the layers\n",
    "        write_images=False,  # whether to write model weights to visualize as image in TensorBoard.\n",
    "    )\n",
    "                         \n",
    "    history = model.fit(x_train, y_train,\n",
    "                        epochs=epochs,\n",
    "                        batch_size=batch_size,\n",
    "                        verbose=1,\n",
    "                        validation_split=0.1,\n",
    "                        callbacks=[checkpoint, reduce_lr, tensorboard_callback]\n",
    "                       )\n",
    "    return history"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let us initiate the training process\n",
    "history = train(model, x_train, y_train, epochs=60)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Investigating model training history\n",
    "model.fit() returned an object containing the training history. Run below cell to plot the training history in terms of loss and accuracy metrics for the training history. However, you may want to compare the actual performance and convergence characteristics with previous runs. [TensorBoard](https://www.tensorflow.org/tensorboard) is a tool for providing the measurements and visualizations needed during the machine learning workflow. Besides tracking experiment metrics like loss and accuracy it allows visualizing the model graph, projecting embeddings to a lower dimensional space, and much more.\n",
    "\n",
    "**Notes:**\n",
    "* Use the \"update\" button on the tensorboard extension (top right) if you have trained a new model\n",
    "* The jupyter tensorboard-plugin may not give you the best overview. Run '''tensorboard --logdir your/log/directory''' in a terminal to open tensorboard in its own tab."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot the training history as loss and accuracy curves\n",
    "acc = history.history['accuracy']\n",
    "val_acc = history.history['val_accuracy']\n",
    "loss = history.history['loss']\n",
    "val_loss = history.history['val_loss']\n",
    "\n",
    "epochs = range(1, len(acc) + 1)\n",
    "\n",
    "_, ax = plt.subplots(ncols=2, figsize=(15, 6))\n",
    "ax[0].plot(epochs, loss, 'bo', label='Training loss')\n",
    "ax[0].plot(epochs, val_loss, 'g', label='Validation loss')\n",
    "ax[0].set_title('Training and validation loss')\n",
    "ax[0].legend()\n",
    "\n",
    "ax[1].plot(epochs, acc, 'bo', label='Training acc')\n",
    "ax[1].plot(epochs, val_acc, 'g', label='Validation acc')\n",
    "ax[1].set_title('Training and validation accuracy')\n",
    "ax[1].legend();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext tensorboard\n",
    "%tensorboard --logdir $log_basedir  # start tensorboard with ipython magic command"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluation after Training"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the model and parameters with the best validation accuracy during training.\n",
    "# This works also if you interruped the training!\n",
    "model = tf.keras.models.load_model(model.name + '.keras')\n",
    "# Note: You can also load ONLY the parameters of the model from file if you still have the model in working memory.\n",
    "# model.load_weights(model.name + '.h5')\n",
    "# When saving the model, you can experiment with models in the code and come back to old models.\n",
    "model.summary()\n",
    "print(model.optimizer.get_config())  # plot the compilation settings"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Evaluate the performance of the network on an indepdendent test set.\n",
    "test_loss, test_acc = model.evaluate(x_test, y_test, verbose=0)\n",
    "print(\"Loss on test set:\", test_loss)\n",
    "print(\"Accuracy on test set:\", test_acc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Hyperparameter optimization\n",
    "The results you should have so far are probably not so impressive yet. The performance you have right now is roughly what networks of the late 90s were able to do. Neural networks have made tremendous progress in the last years. We will now get a sense of the methods leading to this progress step by step.\n",
    "\n",
    "**Exercise:**\n",
    "\n",
    "4. Modify the model and inspect the effect on the accuracy while keeping the number of weights below 100000.\n",
    "Suggested modifications:\n",
    "    * non-linear [activation functions](https://keras.io/api/layers/activations/). Generically discuss the pros and cons of different activation functions (later in the protocol). After choosing an activation function use the non-linear function in all further experiments.\n",
    "    * [Batch normalization](https://www.tensorflow.org/api_docs/python/tf/keras/layers/BatchNormalization),  [theory introduction:](https://arxiv.org/pdf/1502.03167.pdf). After seeing the impact of batch normlalization use batch normalization in all further experiments.\n",
    "    * number of dense layers\n",
    "    * number of neurons per layer\n",
    "\n",
    "Try to get the accuracy over at least 0.97."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "env2",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.15"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
