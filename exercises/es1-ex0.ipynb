{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<p style=\"text-align:right;\">Daniel Böckenhoff, Max-Planck-Institute for Plasma Physics</p>\n",
    "\n",
    "# Linear Regression Example <span style=\"font-size: small\">(a very special case of a neural network)</span>\n",
    "\n",
    "## Bike Sharing Demand\n",
    " - data set of *bike rental counts* in a Washington, D.C., bikeshare program \n",
    " - much simplified data table with only\n",
    "    - ```temp``` - temperature in Celsius\n",
    "    - ```count``` - number of total rentals\n",
    "    for individual hours during daytime and summer\n",
    "    - other features like humidity, weekday, holiday and time of day ignored\n",
    " - **task**: predict bike rental demand (count of bikes) based on temperature\n",
    " - data source: [kaggle](https://www.kaggle.com/c/bike-sharing-demand)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Import libraries\n",
    " - [Pandas](https://pandas.pydata.org) for reading input tables from a file to a *data frame*.\n",
    " - [Matplotlib](https://matplotlib.org) for making plots.\n",
    " - [TensorFlow](https://www.tensorflow.org) to compute gradients and general machine learning."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import tensorflow as tf\n",
    "import math"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# load the training data from the table file to a pandas data frame\n",
    "df = pd.read_csv(\"../data/bikes-summerdays.tbl\", sep = '\\s+') # cols are split by at least one white space\n",
    "print(df.head()) # show first lines as examples\n",
    "\n",
    "# convert count data from integer to float as regression target\n",
    "df['count'] = df['count'].astype(float) \n",
    "print(df.dtypes)\n",
    "\n",
    "m, n_cols = df.shape # training set size and number of columns \n",
    "print(\"m = \", m, \"\\tn_cols = \", n_cols)\n",
    "\n",
    "# compute average of hourly bike rental counts\n",
    "meancount = np.mean(df['count'])\n",
    "print (\"mean bike count = \", meancount)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Make a first explorative plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# uncomment the next line to make the plot interactive (works at brain only in classic mode)\n",
    "# %matplotlib notebook\n",
    "# make the plots static images again by uncommenting and executing this line \n",
    "# %matplotlib inline\n",
    "\n",
    "def scatterplot_rentals():\n",
    "    ''' Plot the bike rental counts versus the temperature.'''\n",
    "    _, ax = plt.subplots(figsize = (10, 6)) # width x height\n",
    "    ax.scatter(df[\"temp\"], df[\"count\"])\n",
    "    plt.title(\"bike rentals versus temperature\")\n",
    "    plt.xlabel('temperature [' + r'$^\\circ$' + 'C]')\n",
    "    plt.ylabel('rental count per hour');\n",
    "    return ax\n",
    "\n",
    "ax = scatterplot_rentals();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# print summary statistics \n",
    "df.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear Regression\n",
    "\n",
    "Input variables $x_1,\\ldots, x_n$ (<span style=\"color:red\">features</span>). Here: $n=1$ and $x_1=$ temperature.\n",
    "\n",
    "Real valued output variable $y$ (aka <span style=\"color:red\">response</span> variable). Here, $y=$ bike count.\n",
    "\n",
    "### Hypothesis function\n",
    "Let ${\\bf x}=(x_1,\\ldots, x_n)$ be an input vector and \n",
    "$$\\pmb{\\theta} = \\begin{pmatrix}\\theta_0\\\\\\vdots\\\\ \\theta_n\\end{pmatrix}$$\n",
    "\n",
    "be a vector of unknown coefficients (<span style=\"color:red\">parameters</span>). The function\n",
    "\n",
    "$$h_{\\pmb{\\theta}}({\\bf x}) := \\theta_0 + \\theta_1 x_1 + \\cdots \\theta_n x_n = (1, x_1, \\cdots, x_n) \\cdot \\pmb{\\theta}$$\n",
    "\n",
    "is called the <span style=\"color:red\">hypothesis</span>.\n",
    "\n",
    "**Aim**: Find parameters $\\pmb{\\theta}$ such that, roughly speaking,\n",
    "$h_{\\pmb{\\theta}}({\\bf x}) \\approx y$\n",
    "for most *examples* $({\\bf x}, y)$ from a data set or data distribution.\n",
    "\n",
    "### Data set\n",
    "Let $m$ be the number of training examples (pairs of input/output), \n",
    "\n",
    "$${\\bf X} := \\begin{pmatrix}\n",
    "1 & x^{(1)}_1 & x^{(1)}_2 & \\cdots & x^{(1)}_n\\\\\n",
    "\\vdots & \\vdots & \\vdots & &\\vdots\\\\ \n",
    "1 & x^{(m)}_1 & x^{(m)}_2 & \\cdots & x^{(m)}_n\\\\\n",
    "\\end{pmatrix} = \\left(x^{(i)}_j\\right)_{\\tiny\\begin{array}{l}1\\le i\\le m\\\\ 0\\le j \\le n\\end{array}\n",
    "},$$\n",
    "\n",
    "be the *data matrix*, where $x^{(i)}_j$ is the value of the $j$-th feature of the $i$-th example, $x_0^{(j)}:=1$, and \n",
    "\n",
    "$${\\bf y} = \\begin{pmatrix}y_1\\\\\\vdots\\\\y_m\\end{pmatrix}$$\n",
    "\n",
    "be the vector of responses.\n",
    "\n",
    "\n",
    "### Mean squared error function\n",
    "Define the *mean squared error* function\n",
    "\n",
    "$$\\text{MSE}(\\pmb{\\theta}) = \\frac{1}{m} ({\\bf X} \\pmb{\\theta} - {\\bf y})^2  \n",
    "= \\frac{1}{m} \\sum_{i=1}^m \\left(h_{\\pmb{\\theta}}({\\bf x}^{(i)}) - y_i\\right)^2, \\qquad \\qquad (1)$$\n",
    "\n",
    "where the square is meant to be applied componentwise to a vector. In machine learning (ML), $\\text{MSE}$ is called a\n",
    "<span style=\"color:red\">loss function</span>. It is our target to be minimized:\n",
    "\n",
    "$$ \\text{MSE}(\\pmb{\\theta}) \\to \\min$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create Data Matrix $X$ and response vector $y$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "# extract response vector\n",
    "y_train = np.array(df['count']) \n",
    "\n",
    "# extract feature columns\n",
    "n = n_cols - 1 # number of features\n",
    "temps = np.array(df.loc[:, df.columns != 'count']) # matrix of all other columns (here only one col)\n",
    "\n",
    "# make data matrix X\n",
    "X_train = np.ones((m, n+1)) # initialize with all ones\n",
    "# overwrite all but the zero-th column with features\n",
    "\n",
    "# normalize temperatures so they are roughly in [-1,1] \n",
    "X_train[:, 1:n+1] = temps / 10 - 2.5\n",
    "\n",
    "# print first 5 examples\n",
    "print(\"X_train:\\n \", X_train[0:5,], \"\\ny_train :\" , y_train[0:5])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define Loss Function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# mean squared error loss function\n",
    "def MSE(theta):\n",
    "    \"\"\"\n",
    "        Mean squared error function for linear regression\n",
    "        theta: numpy array of parameters, the first dimension must match the number of cols of X\n",
    "               If theta is 2-dimensional, the output is 1-dim with one entry per col of theta.\n",
    "    \"\"\"\n",
    "    if len(theta.shape) == 1: # theta is 1-dimensional\n",
    "        theta = tf.reshape(theta, (-1, 1)) # make it a matrix with one column\n",
    "    # now theta is a matrix in any case\n",
    "    yhat = tf.linalg.matmul(X_train, theta) # vector of predicted rental counts\n",
    "    d = (yhat - y_train.reshape((-1, 1)))**2 # square the residuals ('errors')\n",
    "    E = tf.reduce_sum(d, axis=0) / m\n",
    "    return E"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# two tests of calls to error function\n",
    "print(\"single point:\", MSE(np.array([1., 2.])))\n",
    "print(\"two points: \", MSE(np.array([[1., 3.], [2., 4.]])))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "# this is for lecture purposes only and NEED NOT BE READ\n",
    "def contourPlot():\n",
    "    ''' Plot the error landscape'''\n",
    "    # compute error for the grid of all combinations of theta0 and theta1 values\n",
    "    theta0 = theta1 = np.arange(-3200., 3200., 100) # grid axis ranges\n",
    "    xv, yv = np.meshgrid(theta0, theta1) # x and y values of all grid points\n",
    "    Theta = np.array([xv, yv]).reshape(2, -1) # 2 rows, one col per grid point\n",
    "\n",
    "    # compute error for all grid points\n",
    "    z = MSE(Theta).numpy()\n",
    "    z = z.reshape((theta0.size, -1)) # make this a matrix again as required by contour\n",
    "\n",
    "    ## make contour plot\n",
    "    _, axC = plt.subplots(figsize = (9, 8))\n",
    "    # heights to draw contour lines for\n",
    "    h = [50000, 200000] + list(range(500000, 6000001, 500000))\n",
    "    contours = axC.contour(theta0, theta1, z, levels=h, colors='black')\n",
    "    axC.clabel(contours, inline=True, fontsize=8, fmt='%i') # labels on lines\n",
    "    plt.title(\"mean squared error \" + r'$E(\\theta)$');\n",
    "    plt.xlabel(r'$\\theta_0$')\n",
    "    plt.ylabel(r'$\\theta_1$');\n",
    "    return axC"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "axC = contourPlot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Gradient and Gradient Descent\n",
    "Gradient descent is a simple optimization algorithm, where the parameters are iteratively updated, by moving each time a (small) step into the direction of the steepest descend of the target function to be minimized:\n",
    "$$ \\pmb{\\theta} \\leftarrow \\pmb{\\theta} - \\alpha \\cdot\\nabla \\;\\text{MSE}(\\pmb{\\theta})\n",
    "\\qquad \\qquad \\qquad (2)$$\n",
    "\n",
    "Here, \n",
    "$$\\nabla\\; \\text{MSE}(\\pmb{\\theta}) = \\left(\\frac{\\partial\\, \\text{MSE}(\\pmb{\\theta})}{\\partial\\, \\theta_0} , \n",
    "\\ldots, \\frac{\\partial\\, \\text{MSE}(\\pmb{\\theta})}{\\partial\\, \\theta_n}\\right)^T$$ \n",
    "is the *gradient* of the mean squared error function in the point $\\pmb{\\theta}$ and $\\alpha$ is called the *learning rate*. \n",
    "\n",
    "The rationale (not guaranteed) is that $\\pmb{\\theta}$ may converge to a *local* optimum. \n",
    "In applications other than linear regression a local optimum may not even be global.\n",
    "In ML practice, usually a little more sophisticated methods are used, but they also require iterative updates based on the gradient."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solution 1: low-level approach\n",
    "Let us first (but never later) do it with a simple self-implemented gradient descent."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def gradient_descent(theta_init, learning_rate):\n",
    "    ''' Simple gradient descent algorithm.'''\n",
    "    N = 100 # number of update steps\n",
    "    alpha = learning_rate; # learning rate\n",
    "    \n",
    "    # create variable for model parameters (weights) and initialize it\n",
    "    theta = tf.Variable(theta_init, dtype = tf.float64, name = \"weights\", trainable = True)\n",
    "    \n",
    "    # variable to monitor progress\n",
    "    err = np.zeros(N + 1) # error after each update step\n",
    "    Theta = np.zeros((2, N + 1)) # sequence of parameters after each step\n",
    "    \n",
    "    # simple gradient descent loop\n",
    "    for i in range(N + 1):        \n",
    "        with tf.GradientTape() as tape: # to be explained and practiced later\n",
    "            E = MSE(theta)\n",
    "        \n",
    "        Theta[:, i] = theta.numpy()\n",
    "        err[i] = E.numpy().squeeze()\n",
    "        if i < N+1:\n",
    "            grad = tape.gradient(E, theta) # let tf compute the derivate of E wrt to theta\n",
    "            theta.assign(theta - alpha * grad) # core update step from formula (2)\n",
    "            if (i%10 == 0): # output some algorithm progress\n",
    "                print (\"error after \", i, \" iterations: \", err[i], \" gradient= \", grad.numpy())\n",
    "    return [Theta, err]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualization of optimization progress"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_error(err, col):\n",
    "    ''' Plot error over time'''\n",
    "    plt.subplots()\n",
    "    plt.plot(np.log(err), 'o-', color=col, linewidth=.2, markersize=3, mfc='none')\n",
    "    plt.title(\"Log error by iteration of gradient descent\")\n",
    "    plt.xlabel('iteration '+r'$i$')\n",
    "    plt.ylabel(r'$ln \\;E(\\theta^{(i)})$');\n",
    "\n",
    "def plot_progress(Theta, err, col, ):\n",
    "    '''Plot error and parameters over time'''\n",
    "    plot_error(err, col)\n",
    "    # enter parameter trajectory to above contour plot\n",
    "    axC = contourPlot()\n",
    "    plt.plot(Theta[0, 0], Theta[1, 0], 'o', color='green', mfc='none') # mark starting point with circle\n",
    "    ymin, ymax = axC.get_ylim()\n",
    "    xmin, xmax = axC.get_xlim()\n",
    "    for i in range(1, Theta.shape[1]):\n",
    "        if (np.abs(Theta[0, i] - Theta[0, i-1]) + np.abs(Theta[1, i] - Theta[1, i-1]) > 1e-6):\n",
    "            # above condition avoids arrows with a length of 0 pixels\n",
    "            plt.arrow(Theta[0, i-1], Theta[1, i-1], Theta[0, i] - Theta[0, i-1], Theta[1, i] - Theta[1, i-1],\n",
    "                      color=col, width=3, head_length=20, head_width=50, overhang=.9, length_includes_head=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A <span style=\"color:darkgreen\">successful</span> run of gradient descent"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "theta_init = np.array([2000., 3000.])\n",
    "# a better starting value for theta would be [meancount, 0]\n",
    "\n",
    "[Theta1, err1] = gradient_descent(theta_init, learning_rate = 0.25) \n",
    "print(\"theta after optimization: \", Theta1[:, -1])\n",
    "plot_progress(Theta1, err1, col='green');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### An <span style=\"color:red\">unsuccessful</span> run of gradient descent\n",
    "If the learning rate is too high, a *local optimum* may not be found. \n",
    "If the learning rate is too small, learning can take a (prohibitively) long time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "[Theta2, err2] = gradient_descent(theta_init, learning_rate = 1.0) # this learning rate is a little too high \n",
    "print(\"theta after optimization: \", Theta2[:, -1])\n",
    "plot_progress(Theta2, err2, col='red');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solution 2: high-level approach\n",
    "A high-level approach is recommended for most practical applications. For this, TensorFlow 2 uses a build-in Keras library with common ML functions and model components."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "optimizer = tf.keras.optimizers.SGD(learning_rate = 0.01, momentum = 0.8)\n",
    "# SGD: stochastic gradient descent\n",
    "\n",
    "loss_object = tf.keras.losses.MeanSquaredError()\n",
    "# This common and preimplemented loss implements the MSE function defined in equation (1)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hide_input"
    ]
   },
   "source": [
    "### Stochastic Gradient Descent\n",
    "- Heuristics for minimizing some error or loss function $E(\\pmb{\\theta}; ({\\bf X},{\\bf y}))$ with respect to $\\pmb{\\theta}$ more efficiently than with regular gradient descent (Formula (2)), in particular for large training data sets $({\\bf X},{\\bf y})$.\n",
    "- Also iteratively updates parameters $\\pmb{\\theta}$ on the basis of the gradient $\\nabla E$, but the gradient is computed only for a (small) sample $(Xbatch, Ybatch)$ of the training data. The number of examples in the batch are called the **batch size**.\n",
    "- The **learning rate** $\\alpha$ plays a similar role as in regular gradient descent: The larger $\\alpha$, the larger the changes to $\\pmb{\\theta}$ in each step.\n",
    "- $0\\le \\mu \\le 1$ is called **momentum**. If $\\mu = 0$, the update step is like in regular gradient descent, except that the gradient is computed on a batch only. With increasing $\\mu$ the update steps are larger and fluctuate less.\n",
    " > ${\\bf v} \\leftarrow {\\bf 0}$  \n",
    " > While the termination condition is not satisfied\n",
    " >> Sample a random batch $(\\text{Xbatch},\\text{Ybatch})$ from the training data $({\\bf X},{\\bf y})$.  \n",
    " >> ${\\bf v} \\leftarrow \\mu \\cdot {\\bf v} - \\alpha \\cdot \\nabla E(\\pmb{\\theta};\\text{Xbatch},\\text{Ybatch}))$  \n",
    " >> $\\pmb{\\theta} \\leftarrow \\pmb{\\theta} + {\\bf v}$ \n",
    "  \n",
    "- The termination condition could be\n",
    "   1. A fixed number of iterations has been done.\n",
    "   2. The accuracy/error on a separate validation set satisfies some condition (e.g. it is plateauing).\n",
    "   3. User interruption.\n",
    "- Usually, the random choices of batches are such that the number of times each example appeared in a batch differs at most by 1, i.e. the batch is drawn from the subset of examples used hitherto one less than the maximum. Once all examples have been used once, an **epoch** ends.\n",
    "- Documentation and Code of [tf.keras.optimizers.SGD](https://www.tensorflow.org/versions/r2.0/api_docs/python/tf/keras/optimizers/SGD)\n",
    "  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(1) # make it reproducible\n",
    "dataset = tf.data.Dataset.from_tensor_slices((X_train, y_train))\n",
    "# A tf.data.Dataset has commonly used functions for random sampling, obtaining subsets, transformations\n",
    "# iterating over large numbers of images on disk (using TFRecord).\n",
    "\n",
    "batch_size = 32\n",
    "dataset = dataset.shuffle(m).batch(batch_size) # random order, use whole dataset as 'batch' for comparability"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get a predefined linear model with one single output variable (unit) and one weight per input.\n",
    "model = tf.keras.Sequential()\n",
    "model = tf.keras.layers.Dense(\n",
    "    units = 1, input_dim = 2,\n",
    "    use_bias = False, # bias equivalent to adding x_0 := 1\n",
    "    kernel_initializer = tf.initializers.Constant(theta_init), # default would be random initialization\n",
    "    dtype = 'float64')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# one gradient descent step\n",
    "def train_step(x, y):\n",
    "    with tf.GradientTape() as tape:\n",
    "        t = model(x) # predicted rental demand\n",
    "        t = tf.reshape(t, [-1])\n",
    "        E = loss_object(y, t)\n",
    "    grads = tape.gradient(E, model.trainable_variables)\n",
    "    # this makes a parameter update using the gradient\n",
    "    optimizer.apply_gradients(zip(grads, model.trainable_variables))\n",
    "    return E"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 5\n",
    "total_batches = N * math.ceil(m / batch_size)\n",
    "Theta3 = np.zeros((2, total_batches)) # sequence of parameters after each step\n",
    "err3 = np.zeros((total_batches)) # high-level approach error after each update step\n",
    "i = 0 # batch number\n",
    "\n",
    "for epoch in range(N):\n",
    "    # loop over batches of dataset\n",
    "    # Here it is here just a single big batch for comparability with Solution 1.\n",
    "    for (x, y) in dataset:\n",
    "        # x: batch features of shape (batch_size, 2)\n",
    "        # y: batch labels\n",
    "        E = train_step(x, y)\n",
    "        Theta3[:, [i]] = model.get_weights()[0]\n",
    "        err3[i] = E\n",
    "        i = i + 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_progress(Theta3, err3, col='blue');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks:**\n",
    "  - In above plot of training error, the errors were measured only on a batch and are thus estimates of the training error only.\n",
    "  - When using stochastic gradient descent, typically the parameter trajectory eventually random walks near a local optimum and their latest values may result in a larger loss than some parameter value previously attained."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# final loss and prediction computed on all training data\n",
    "pred = model(X_train)\n",
    "pred = tf.reshape(pred, [m])\n",
    "E = loss_object(y_train, pred)\n",
    "print(\"final error = \", E.numpy())\n",
    "print(\"theta:\\n\", model.trainable_variables[0].numpy())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plot predictions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# draw the same scatterplot as above\n",
    "ax = scatterplot_rentals()\n",
    "\n",
    "# now add the predictions, here a regression line\n",
    "ax.plot(temps, pred, 'rx-', label = r\"$\\theta_0 + \\theta_1 X$\") # - draws line connecting (r)ed crosses (x)\n",
    "ax.legend(loc = 'upper left');"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
