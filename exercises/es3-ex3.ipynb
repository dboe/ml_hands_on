{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p style=\"text-align:right;\">Daniel Böckenhoff, Max-Planck-Institute for Plasma Physics</p>\n",
    "\n",
    "## What Gets Neurons Excited?\n",
    "In this notebook you will study what features of an image lead to a large activation of a particular unit in an intermediate layer of a convolutional neural network.\n",
    "\n",
    "<img src=\"../figs/exciting-patches.png\" style=\"width:500px\"/>\n",
    "Above image shows five examples of patches of the example images that maximized the values of five different neurons of an intermediate layer.\n",
    "This analysis is important  \n",
    "\n",
    "  * to obtain an intuition how CNNs achieve their performance and\n",
    "  * for *transfer learning*, where the *output of an intermediate layer* is used as an *input* to another neural network.\n",
    "  \n",
    "With transfer learning, you can leverage a pre-trained publicly availabe model that was trained for one purpse, e.g. general purpose image classification, to your specific problem, e.g. distinguishing between photos of your own family members."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "import math\n",
    "import tensorflow as tf\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import cv2\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load a Pretrained Image Classification CNN (Cats, Dogs, Wolves, Plants)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load the model from the drive\n",
    "frozen_model = tf.keras.models.load_model('../nets/animals_and_plants-frozen.h5')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Image Data Preparation - Make Data Frames\n",
    "We will create Pandas data frames to hold the file names and class labels. These data structures are small as they do not contain the actual image data. The images are on the drive and never are they all loaded to memory, so this approach scales to very large training sets of images."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# directory with image class subdirectories\n",
    "img_path = '../data/cats-dogs-plants'\n",
    "\n",
    "# Create a pandas dataframe from a tab separated file \n",
    "df = pd.read_csv(img_path + \"/classes-and-fnames.txt\", sep = '\\t', names = ['classname', 'fname'])\n",
    "df['path'] = img_path + '/' + df['classname'] + \"/\" + df['fname']\n",
    "\n",
    "# take only a sample to reduce memory consumption on brain during class\n",
    "# reduce this further if you get an error \"Out of memory\"\n",
    "# df = df.sample(4000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Make TensorFlow Datasets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "img_size = 96           # width and height of all images (resize, if required)\n",
    "num_imgs  = df.shape[0] # total number of examples\n",
    "print(\"number of images:\", num_imgs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Make tf.Dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def path_to_array(filename):\n",
    "    \"\"\" Map a filename to an actual image tensor using image augmentation, one-hot encode label.\"\"\"\n",
    "    img = tf.io.read_file(filename)\n",
    "    img = tf.image.decode_png(img, channels = 3)\n",
    "    img = tf.image.resize(img, [img_size, img_size]) / 255.\n",
    "    return img"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make a tf dataset of images from a pd data frame of file paths\n",
    "# first, make dataset with just the path string\n",
    "ds_path = tf.data.Dataset.from_tensor_slices(df['path'])\n",
    "\n",
    "# convert to data set with actual images\n",
    "ds = ds_path.map(path_to_array)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# obtain all images as a single tensor x\n",
    "x = None\n",
    "for x_ in ds.batch(num_imgs): # make a fake loop with one iteration as tf.Dataset's apparently have not function for that\n",
    "    x = x_.numpy()\n",
    "    break\n",
    "    \n",
    "print(x.shape, x.dtype)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Delete large unneccessary variables as the code below needs a lot of memory and we only need x.\n",
    "del ds, df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Make a model that outputs intermediate layer's activations from ```frozen_model```\n",
    "First read in the model and let's assume we didn't know the model architecture (it is from ```cnn-class-cats.ipynb```)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# print out a summary of the model ...\n",
    "frozen_model.summary()\n",
    "\n",
    "# ... as well as details on the kernel sizes and strides\n",
    "print(\"Conv2d and MaxPool2Dlayers:\\nlayer num, name, kernel size and strides\")\n",
    "formatstr = \"{:>2d}  {:16s} {:10s} {:10}\"\n",
    "for i, layer in enumerate(frozen_model.layers):\n",
    "    if isinstance(layer, tf.keras.layers.Conv2D):\n",
    "        print(formatstr.format(i, layer.name, str(layer.kernel_size), str(layer.strides)))\n",
    "    if isinstance(layer, tf.keras.layers.MaxPool2D):\n",
    "        print(formatstr.format(i, layer.name, \"  -\", str(layer.strides)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Chose a target layer and create a model that computes its activation (\"feature map\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# we here chose the output of the 6th convolutional layer\n",
    "target_layer_name = \"conv2d_5\"\n",
    "\n",
    "# create feature map: a function that maps an image to the activation (value) of the target layer's units\n",
    "layer_output = frozen_model.get_layer(target_layer_name).output\n",
    "feature_map = tf.keras.models.Model(inputs = frozen_model.input, outputs=layer_output)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compute the activation for the ~7000 images"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "layer_act = feature_map.predict(x) # takes ~1m\n",
    "layer_shape = layer_act.shape\n",
    "layer_shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What gets neurons excited?\n",
    "Each of the 64 channels $c$ of the target layer activation contains output of a function that maps square image patches from the input to the value of an activation in channel $c$ of the target layer. To see what kind of input leads to a large activation, we simply go through all such patches from all our images and take note the ones that produced the highest activations. Frequently, when a square image patch \"excites\" a neuron, neighboring image patches that largely overlap the first also exite that neuron. To obtain independent results, we allow *at most one* such patch per image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# set the number to example image patches to see per channel\n",
    "k_root = 4\n",
    "k = k_root**2 # find the k most activating image patches, chosen as a square for better visialization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# first maximize over the width and height of the layer\n",
    "# move these two dimensions to the front, then flatten them to one dimension\n",
    "T = layer_act.transpose((1, 2, 0, 3)).reshape([-1, layer_shape[0], layer_shape[3]])\n",
    "print(\"T\", T.shape)\n",
    "\n",
    "# find the \"image\" coordinates, that maximize activation\n",
    "# for each photo and channel of the target layer\n",
    "img_flat_ind = np.argmax(T, axis = 0)\n",
    "R = np.max(T, axis = 0)\n",
    "print(\"R\", R.shape, \"\\nimg_flat_ind\", img_flat_ind.shape)\n",
    "\n",
    "# efficiently (linear time) find the k images with the largest maximal activations \n",
    "bb = np.argpartition(R, -k, axis = 0)[-k:] # for each channel: which k images have the highest activating patches?\n",
    "print(\"indices to image bb\", bb.shape)\n",
    "\n",
    "# now get the row (i) and col (j) coordinates of the k best images for each channel\n",
    "S = np.zeros_like(bb)\n",
    "for c in range(S.shape[1]): # loop over channels\n",
    "    subset = bb[:, c]\n",
    "    S[:, c] = img_flat_ind[subset, c] # use fancy indexing\n",
    "print(\"S\", S.shape)\n",
    "\n",
    "# map indices from the flattened array back to image coordinate pairs\n",
    "ii, jj = np.unravel_index(S, layer_shape[1:3]) \n",
    "print (\"indices to row ii \", ii.shape)\n",
    "del T, img_flat_ind, R, S, layer_act # let the kernel free the memory"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " ### Determine the origin patches position and size for any unit in the intermediate layer\n",
    " \n",
    "<img size=\"1900\" src=\"../figs/es3-ex2-sol2.png\"/>\n",
    "\n",
    " **Exercise:** Figure this out with pen and scratch paper and then enter the correct numbers manually below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# ur_size is the height and width of a square patch of the original image\n",
    "# that a single unit of layer conv2d_5 (shape: 39, 39, 64) depends on, say ur-patch\n",
    "ur_size = 20 # YOUR VALUE HERE\n",
    "\n",
    "# If you shift one unit to the right (or left) in layer conv2d_5, the upper left corner of the ur-patch shifts by scaling_factor units to the right (left)\n",
    "scaling_factor = 2 # YOUR VALUE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot the maximizing image patches for each channel\n",
    "Below plot shows the $k=16$ best fitting images for each channel of the first ```2 * num_fig_rows``` channels of the 8-th layer (```conv2d_5```). What do they appear to recognize? Remember, that a single unit in a convolutional layer gets input from **all channels** of the previous layer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "num_fig_rows = 32 # must be below 32 = layer_shape[3] / 2\n",
    "\n",
    "# plot_feature_map\n",
    "# display the patches for 2 channels in one figure \"row\"\n",
    "fig, ax = plt.subplots(nrows = num_fig_rows * (k_root + 1) - 1, ncols = 2 * k_root + 1, figsize = (18, num_fig_rows * 8 - 2))\n",
    "[axi.set_axis_off() for axi in ax.ravel()]\n",
    "for c in range(2 * num_fig_rows):\n",
    "    irow_offset = math.floor(c / 2)  * (k_root + 1)\n",
    "    icol_offset = (c % 2) * (k_root + 1)\n",
    "    for r in range(k):\n",
    "        irow = math.floor(r / k_root) + irow_offset\n",
    "        icol = r % k_root + icol_offset\n",
    "        \n",
    "        # scaling_factor used here to find the coordinates (i, j) of the upper left corner of the ur-patch\n",
    "        i = scaling_factor * ii[r, c]\n",
    "        j = scaling_factor * jj[r, c] \n",
    "        \n",
    "        # ur_size used here to find the coordinates of the lower right (lr) pixel of the ur-patch\n",
    "        lr_i = i + ur_size\n",
    "        lr_j = j + ur_size\n",
    "        \n",
    "        if (i >= 0 and lr_i <= img_size and j >= 0 and lr_j <= img_size):\n",
    "            img = x[bb[r, c], i : lr_i, j : lr_j, :]\n",
    "            ax[irow, icol].imshow(img)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
