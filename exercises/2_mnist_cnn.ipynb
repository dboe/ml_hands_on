{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p style=\"text-align:right;\">Daniel Böckenhoff, Max-Planck-Institute for Plasmaphysics</p>\n",
    "\n",
    "## Convolutional Neural Networks to recognise numbers with the MNIST database\n",
    "After we have experienced with fully connected NNs that it is hard to get accuracies of 99% and above, we investigate the power of convolutional neural networks. Finally we will investigate the robustnes of our dataset.\n",
    "\n",
    "**What you will learn:**\n",
    " - Further classification error analysis\n",
    " - inspect failure cases and understand how to approach to make a NN more robust\n",
    " \n",
    "**Task:**\n",
    "Carefully read the cells and execute them (Shift enter or menu panel). Complete the exercises marked as such.\n",
    "  \n",
    "**Note:**\n",
    "You can skip until model definition (\"Define and train a convolutional neural network\") if you have done the mnist_dense exercise."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Image Data Preparation\n",
    "We will load the mnist data set with the *keras.datasets* backend."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pathlib\n",
    "import tensorflow as tf\n",
    "import numpy as np\n",
    "import sklearn.metrics\n",
    "import matplotlib.pyplot as plt\n",
    "import os\n",
    "import datetime"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the path to the directory where we have already downloaded the data\n",
    "data_path = (pathlib.Path(\".\") / \"..\").resolve() / 'data' / 'mnist.npz'  # if no path were given, keras would download the dataset\n",
    "\n",
    "# Load the data\n",
    "data = np.load(data_path)\n",
    "x_train, y_train, x_test, y_test = data['x_train'], data['y_train'], data['x_test'], data['y_test']\n",
    "class_names = list(set(y_train))\n",
    "num_classes = len(class_names)  # 10\n",
    "num_imgs = len(y_train) + len(y_test)\n",
    "\n",
    "print(\"Number of samples in whole data set: {num_imgs}\".format(**locals()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Make TensorFlow Datasets\n",
    "\n",
    "\n",
    "**Note:** Training, Validation and Test Split\n",
    "A rule of thum is to split the data set in taining, validation and test set by the ratio 80:10:10.\n",
    "A specialty in the mnist dataset is the predefined test set which serves the purpose of exact comparability between different research groups."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "img_size = x_train.shape[1]  # width and height of all images\n",
    "\n",
    "# Convert target classes to categorical ones\n",
    "y_train_labels = y_train\n",
    "y_test_labels = y_test\n",
    "y_train = tf.keras.utils.to_categorical(y_train_labels, num_classes)\n",
    "y_test = tf.keras.utils.to_categorical(y_test, num_classes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define and Train a Convolutional Neural Network\n",
    "**Layer types:**\n",
    " - [Conv2D](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Conv2D) convolutional layer, here inputs are 3dim and the convolution sums over the 3rd dim.\n",
    " - [MaxPooling2D](https://www.tensorflow.org/api_docs/python/tf/keras/layers/MaxPooling2D) takes local maxima over a rectangular region.\n",
    " - [Dropout](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Dropout) randomly deactivates neurons during training, which can make training more robust.\n",
    " - [BatchNormalization](https://arxiv.org/pdf/1502.03167.pdf), centralizes and scales its input to have approximately 0 mean and variance 1.\n",
    "\n",
    "**Exercise:**\n",
    "\n",
    "1. Implement a NN architecture similar to that in [this paper](http://yann.lecun.com/exdb/publis/pdf/lecun-01a.pdf). You do not need to read the full paper. Look at figure 2 and use that as inspiration. For more detail you may read Section II.B *LeNet-5*. In case you do not have access to that paper, you find it under *literature/lecun-01a.pdf* in this git repository.\n",
    "\n",
    "**Links:**\n",
    "* [tf.keras.layers.Reshape](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Reshape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "task"
    ]
   },
   "outputs": [],
   "source": [
    "np.random.seed(1)  # so we all get the same pseudorandom results\n",
    "\n",
    "model = tf.keras.models.Sequential(name=\"mnist_cnn_student\")  # sequential stack of layers\n",
    "\n",
    "model.add(tf.keras.layers.Input((img_size, img_size)))\n",
    "model.add(tf.keras.layers.BatchNormalization())\n",
    "\n",
    "############################################################\n",
    "# Your modifications to the model here\n",
    "############################################################\n",
    "\n",
    "model.add(tf.keras.layers.Dense (num_classes, activation = \"softmax\"))\n",
    "\n",
    "# Configure the model and start training\n",
    "model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])\n",
    "\n",
    "# pretty print a summary of the layer types and dimensions\n",
    "model.summary()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "log_basedir = os.path.join(\"logs\", \"mnist_cnn/\")\n",
    "\n",
    "def train(model, x_train, y_train, epochs=20, batch_size=250, log_basedir=log_basedir):\n",
    "    # Callbacks: What should be done during (long) training?\n",
    "\n",
    "    # Function to store model to file, if validation loss has a new record\n",
    "    # Check always after having seen at least another save_freq examples.\n",
    "    checkpoint = tf.keras.callbacks.ModelCheckpoint(\n",
    "        model.name + '.keras',\n",
    "        save_weights_only=False,  # save the entire model, not only the weights. This allows re-building the whole model\n",
    "        monitor='val_loss',\n",
    "        mode='min',\n",
    "        save_best_only=True,\n",
    "        verbose=1)\n",
    "\n",
    "    # Function to decrease learning rate by 'factor'\n",
    "    # when there has been no significant improvement in the last 'patience' epochs.\n",
    "    reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(\n",
    "        monitor='val_loss', mode='min', factor=0.75, patience=3, verbose=1)\n",
    "\n",
    "    # Use tensorboard to show the progress. See cell below for instructions on how to use tensorboard\n",
    "    log_dir = log_basedir + datetime.datetime.now().strftime(\"%Y%m%d-%H%M%S\") + \"-\" + model.name\n",
    "\n",
    "    tensorboard_callback = tf.keras.callbacks.TensorBoard(\n",
    "        log_dir,\n",
    "        histogram_freq=1,  # frequency (in epochs) at which to compute activation and weight histograms for the layers\n",
    "        write_images=False,  # whether to write model weights to visualize as image in TensorBoard.\n",
    "    )\n",
    "                         \n",
    "    history = model.fit(x_train, y_train,\n",
    "                        epochs=epochs,\n",
    "                        batch_size=batch_size,\n",
    "                        verbose=1,\n",
    "                        validation_split=0.1,\n",
    "                        callbacks=[checkpoint, reduce_lr, tensorboard_callback]\n",
    "                       )\n",
    "    return history"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let us initiate the training process\n",
    "history = train(model, x_train, y_train, epochs=100)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot the training history as loss and accuracy curves\n",
    "acc = history.history['accuracy']\n",
    "val_acc = history.history['val_accuracy']\n",
    "loss = history.history['loss']\n",
    "val_loss = history.history['val_loss']\n",
    "\n",
    "epochs = range(1, len(acc) + 1)\n",
    "\n",
    "_, ax = plt.subplots(ncols = 2, figsize = (15, 6))\n",
    "ax[0].plot(epochs, loss, 'bo', label = 'Training loss')\n",
    "ax[0].plot(epochs, val_loss, 'g', label = 'Validation loss')\n",
    "ax[0].set_title('Training and validation loss')\n",
    "ax[0].legend()\n",
    "\n",
    "ax[1].plot(epochs, acc, 'bo', label = 'Training acc')\n",
    "ax[1].plot(epochs, val_acc, 'g', label = 'Validation acc')\n",
    "ax[1].set_title('Training and validation accuracy')\n",
    "ax[1].legend();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext tensorboard\n",
    "%tensorboard --logdir ./logs  # start tensorboard with ipython magic command"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluation after Training"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the parameters with the best validation accuracy during training.\n",
    "# This works also if you interruped the training!\n",
    "model.load_weights(model.name + '.keras')\n",
    "test_loss, test_acc = model.evaluate(x_test, y_test, verbose = 0)\n",
    "print(\"Loss on test set:\", test_loss, \"\\nAccuracy on test set:\", test_acc)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# make a prediction\n",
    "prediction = model.predict(x_test)\n",
    "prediction_class = prediction.argmax(axis = 1)\n",
    "prediction_confidence = prediction.max(axis = 1)\n",
    "\n",
    "print(\"Predicted probabilities for first 5 examples:\\n\", np.round(prediction[0:5], 3))\n",
    "print(\"Predicted classes for first 5 examples:\\n\", prediction_class[0:5])\n",
    "print(\"Probability of prediction:\\n\", prediction_confidence[0:5])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sklearn.metrics.confusion_matrix(y_test_labels, prediction_class)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(sklearn.metrics.classification_report(y_test_labels, prediction_class))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Display misclassified numbers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "indices = np.arange(len(y_test))[y_test_labels != prediction_class]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in indices[:5]:\n",
    "    print(\"Expected\", y_test_labels[i], \", got\", prediction_class[i], \"with prob\", prediction_confidence[i])\n",
    "    # Visualize one sample\n",
    "    plt.imshow(x_test[i], cmap='Greys')\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "matplotlib"
    ]
   },
   "source": [
    "## 'Deploy'\n",
    "We now go 'online' with our newly built model and deploy it to make use of it.\n",
    "\n",
    "**Usage:**\n",
    "\n",
    "Run the next cell. A opencv window will pop up. Draw in the canvas with the left mouse button activated. The following commands are available:\n",
    "\n",
    "**Commands:**\n",
    "* r to reset the image\n",
    "* c (calculate) to let the neural network predict the target of your image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "matplotlib"
    ]
   },
   "outputs": [],
   "source": [
    "# No need to understand the code below !!!\n",
    "# %matplotlib notebook\n",
    "np.set_printoptions(formatter={'float': '{: .2f}'.format})\n",
    "\n",
    "img = np.zeros((img_size, img_size), np.uint8)\n",
    "\n",
    "\n",
    "\n",
    "fig = plt.figure()\n",
    "ax = fig.add_subplot(211)\n",
    "artist = ax.imshow(img, cmap='Greys')\n",
    "ax_text = fig.add_subplot(212)\n",
    "ax_text.axis('off')\n",
    "header=ax_text.text(0,\n",
    "                    0.2,\n",
    "                    \"Confidences\" + \" \" * 75 + \"|\" \"   Prediction\",\n",
    "                    va=\"bottom\", ha=\"left\")  # Table head\n",
    "separator=ax_text.text(0,\n",
    "                       0.1,\n",
    "                       \"-\" * (82) + \"+\" + \"-\" * 13,\n",
    "                       va=\"bottom\", ha=\"left\")  # Table separator\n",
    "\n",
    "text=ax_text.text(0, 0, \"\", va=\"bottom\", ha=\"left\")\n",
    "\n",
    "ax.tick_params(\n",
    "    axis='both',       # changes apply to both axes\n",
    "    which='both',      # both major and minor ticks are affected\n",
    "    bottom=False,      # ticks along the bottom edge are off\n",
    "    top=False,         # ticks along the top edge are off\n",
    "    labelbottom=False) # labels along the bottom edge are off\n",
    "\n",
    "drawing = False\n",
    "\n",
    "\n",
    "def update():\n",
    "    artist = ax.imshow(img, cmap='Greys')\n",
    "    artist.set_array(img)\n",
    "    fig.canvas.draw()\n",
    "    fig.canvas.flush_events()\n",
    "\n",
    "\n",
    "def draw(event):\n",
    "    ix, iy = int(round(event.xdata)), int(round(event.ydata))\n",
    "    if img[iy, ix] == 0:\n",
    "        img[iy, ix] = 255\n",
    "        update()\n",
    "\n",
    "def pen_down(event):\n",
    "    global drawing\n",
    "    drawing = True\n",
    "    draw(event)\n",
    "    \n",
    "def pen_up(event):\n",
    "    global drawing\n",
    "    drawing = False\n",
    "\n",
    "def pen_move(event):\n",
    "    global drawing\n",
    "    if drawing:\n",
    "        draw(event)\n",
    "        \n",
    "def keyboard_input(event):\n",
    "    global img\n",
    "    if event.key == 'c':\n",
    "        try:\n",
    "            x_img = img.reshape(1, img_size, img_size)\n",
    "            y_img = model.predict(x_img / 255.)\n",
    "            tx = \"{0}  |  {1}\".format(y_img[0], y_img.argmax(axis=1)[0])\n",
    "        except Exception as err:\n",
    "            tx = str(err)\n",
    "            \n",
    "        text.set_text(tx)\n",
    "    elif event.key == 'r':\n",
    "        img = np.zeros((img_size, img_size), np.uint8)\n",
    "        update()\n",
    "\n",
    "fig.canvas.mpl_connect('button_press_event', pen_down)\n",
    "fig.canvas.mpl_connect('button_release_event', pen_up)\n",
    "fig.canvas.mpl_connect('motion_notify_event', pen_move)\n",
    "fig.canvas.mpl_connect('key_press_event', keyboard_input)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(img, cmap='Greys')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise**:\n",
    "\n",
    "2. Experiment with the above 'deployed' neural net. Can you find systematic weeknesses of the NN? Do you experience the same accuracy as measured by the test set? If not, what could be the reasons and possible solutions to more robust classification?"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "env2",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.15"
  },
  "widgets": {
   "application/vnd.jupyter.widget-state+json": {
    "state": {},
    "version_major": 2,
    "version_minor": 0
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
